package Testcases_Analytics;

import java.awt.AWTException;
import java.io.IOException;

import org.openqa.selenium.By;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import TestBase.Base_app;
import Utils.Testutils;
import modules.Analytics;

public class Testcase_Analytics_All_platform_Metrics_verification extends Base_app
{
	modules.Analytics analytics;
	Testutils testutils;

	public Testcase_Analytics_All_platform_Metrics_verification()
	{
		super();
	}

	@BeforeMethod
	public  void setup() throws IOException, AWTException, InterruptedException
	{
		Base_app.initialization();
		analytics = new Analytics();
	}
	
	@Test(priority=1)
	public void Testcase_Analytics_facebook_clients_metrics() throws InterruptedException, AWTException
	{
		analytics.Testcase_verify_Metrics_Facebook_clients_account();
		System.out.println("Successfully check all clients metrics Facebook");
	}
	
	@Test(priority=2)
	public void Testcase_Analytics_facebook_trial_metrics() throws InterruptedException
	{
		analytics.Testcase_verify_Metrics_Facebook_trial_account();
        System.out.println("Successfully check all trials metrics Facebook");
	}

	@Test(priority=3)
	public void Testcase_Analytics_instagram_clients_metrics() throws InterruptedException
	{
		analytics.Testcase_verify_Metrics_Instagram_clients_account();
		System.out.println(	"Successfully clients Instagram");
	}
	
	@Test(priority=4)
	public void Testcase_Analytics_instagram_trials_metrics() throws InterruptedException
	{
		analytics.Testcase_verify_Metrics_Instagram_trial_account();
		System.out.println(	"Successfully trials Instagram");
	}
	
	@Test(priority=5)
	public void Testcase_Analytics_Twitter_clients_metrics() throws InterruptedException
	{
		analytics.Testcase_verify_Metrics_Twitter_clients_account();
		System.out.println("Successfully  clients Twitter");
	}

	@Test(priority=6)
	public void Testcase_Analytics_Twitter_trials_metrics() throws InterruptedException
	{
		analytics.Testcase_verify_Metrics_Twitter_trial_account();
		System.out.println("Successfully trialls  Twitter");
	}

	@Test(priority=7)
	public void Testcase_Analytics_Youtube_clients_metrics() throws InterruptedException
	{
		analytics.Testcase_verify_Metrics_Youtube_clients_account();
		System.out.println("Successfully clients metrics Youtube");
	}

	@Test(priority=8)
	public void Testcase_Analytics_Youtube_trials_metrics() throws InterruptedException
	{
		analytics.Testcase_verify_Metrics_Youtube_trial_account();
		System.out.println("Successfully trials metrics  Youtube");
	}

	@AfterMethod
	public void teardown() throws InterruptedException
	{
		Thread.sleep(2000);
		driver.findElement(By.xpath("/html/body/div[3]/div[1]/div")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("/html/body/div[3]/div[3]/div[4]/a")).click();
		driver.close();
	}
}