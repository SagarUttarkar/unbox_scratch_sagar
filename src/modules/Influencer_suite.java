package modules;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import TestBase.Base_app;

public class Influencer_suite extends Base_app
{

	WebElement influencer_suite = driver.findElement(By.xpath("//*[@id=\"main-menu-wrapper\"]/ul/li[7]/a"));

	WebElement curated_list = driver.findElement(By.xpath("//*[@id=\"4_\"]/span[1]/a/li"));

	WebElement Filters = driver.findElement(By.xpath("//*[@id=\"ig_filters\"]/div/div[1]/div/span"));

	WebElement add = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div/div/div[1]/div/div[2]/a[1]/div"));

	WebElement campaigns = driver.findElement(By.xpath("//*[@id=\"4_\"]/span[2]/a"));

	WebElement Advance_filter = driver.findElement(By.xpath("//*[@id=\"influencers\"]/div[2]/div[1]/div[1]/span[1]"));
	
	WebElement closePopUp = driver.findElement(By.className("closePopup"));
	
	public Influencer_suite()
	{
		PageFactory.initElements(driver, this);
	}
	JavascriptExecutor js = (JavascriptExecutor) driver;
	WebDriverWait wait = new WebDriverWait(driver, 200);
	
	public void curated_list_Filters() throws InterruptedException
	{
	    influencer_suite.click();
		curated_list.click();
		Thread.sleep(2000);
		add.click();
		Filters.click();
		driver.findElement(By.xpath("//*[@id=\"ig_filters\"]/div/div[1]/div/span")).click();
		Thread.sleep(2000);
		String List_all=driver.findElement(By.xpath("/html/body/div[5]/div[2]/div/div/div[2]")).getText();
		System.out.println("INSTAGRAM FILTERS RESULT="+List_all);
		System.out.println("///////////////////////////////////////////////////////////////////////////////////");
		Thread.sleep(4000);
		
		Filters.click();
		driver.findElement(By.xpath("//*[@id=\"typefilter\"]/li[4]/a")).click();
		Thread.sleep(2000);
		String List_all1=	driver.findElement(By.xpath("/html/body/div[5]/div[2]/div/div/div[2]")).getText();
		System.out.println("YOUTUBE FILTERS RESULT="+List_all1);
	}
	
	public void Add_to_curate_list() throws InterruptedException
	{
		influencer_suite.click();
		curated_list.click();
		add.click();
		driver.findElement(By.xpath("//*[@id=\"ig_infl_list\"]/tr[1]/td[1]/div/img")).click();
		driver.findElement(By.xpath("//*[@id=\"ig_infl_list\"]/tr[2]/td[1]/div/img")).click();
		driver.findElement(By.xpath("//*[@id=\"ig_infl_list\"]/tr[3]/td[1]/div/img"	)).click();
		js.executeScript("window.scrollBy(0,450)");
		driver.findElement(By.xpath("//*[@id=\"ig_infl_list\"]/tr[4]/td[1]/div/img")).click();
		driver.findElement(By.xpath("//*[@id=\"ig_infl_list\"]/tr[5]/td[1]/div/img")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("/html/body/div[5]/div[3]/a[2]")).click();
		Random random = new Random();
		String create_list = "Ravina_Test_List_"+random.nextInt();
		driver.findElement(By.xpath("/html/body/div[9]/div/div/div[2]/div/form/input")).sendKeys(create_list);
		driver.findElement(By.xpath("/html/body/div[9]/div/div/div[3]/button[2]")).click();
	}
	
	public void Search_delete() throws InterruptedException
	{
		// Delete
		influencer_suite.click();
		curated_list.click();
		driver.findElement(By.xpath("/html/body/div[5]/div[2]/div/div/div[2]/div[1]/div/div/span/img[2]")).click();
		Thread.sleep(2000);
		String msg= driver.findElement(By.className("discard-text")).getText();
		System.out.println("The account Deleted !!"+ msg);
		driver.findElement(By.xpath("/html/body/div[8]/div/div/div[3]/button[2]")).click();
	
		//search
		driver.findElement(By.xpath("/html/body/div[5]/div[2]/div/div/div[1]/div/div[2]/span/img")).click();
		driver.findElement(By.xpath("/html/body/div[5]/div[2]/div/div/div[1]/div/div[2]/span/input")).sendKeys("matrix");
		Thread.sleep(2000);
		String SearchData=	driver.findElement(By.xpath("/html/body/div[5]/div[2]/div/div/div[2]")).getText();
		System.out.println(SearchData);
	}
	
	public void influencer_list_metrics() throws InterruptedException
	{
		int rowcounter = 1;
		influencer_suite.click();
		curated_list.click();
		driver.findElement(By.xpath("/html/body/div[5]/div[2]/div/div/div[2]/div[1]/div/div")).click();
		Thread.sleep(2000);
		
		// search
		driver.findElement(By.xpath("//*[@id=\"influencers\"]/div[2]/div[1]/div[3]/div/span/input")).sendKeys("Aashna Hegde");
		String Searchlist =driver.findElement(By.className("curatelist")).getText();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"ig_infl_list\"]/tr/td[9]/a")).click();
		System.out.println(Searchlist);
		
		ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(1));
		
		//driver.close();
		//driver.switchTo().window(tabs2.get(0));
		
		//Analytics tab
		System.out.println("Analytics Tab");
		
		//Followers
		System.out.println("**********Followers**********");
		boolean fol = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[3]/div[1]/div/div[1]/h3")).isDisplayed();
		String followers = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[3]/div[1]/div/div[1]/h3")).getText();
		String followersText = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[3]/div[1]/div/div[2]/span")).getText();
		
		if(fol == true )
		{
			System.out.println("Data loaded successfully!");
			System.out.println(followersText+" : "+followers);
		}
		else
		{
			System.out.println("Data loading failed!");
		}
		
		//Location
		System.out.println("**********Location**********");
		boolean loc = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[3]/div[2]/div/div[1]/h3/span")).isDisplayed();
		String location = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[3]/div[2]/div/div[1]/h3/span")).getText();
		String locationText = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[3]/div[2]/div/div[2]/span")).getText();
		
		if(loc == true )
		{
			System.out.println("Data loaded successfully!");
			System.out.println(locationText+" : "+location);
		}
		else
		{
			System.out.println("Data loading failed!");
		}
		
		//Average Views
		System.out.println("**********Average Views**********");
		boolean avgViews = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[4]/div[1]/div[1]/div/div[1]/h3")).isDisplayed();
		String views = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[4]/div[1]/div[1]/div/div[1]/h3")).getText();
		String avgViewsText = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[4]/div[1]/div[1]/div/div[2]/span")).getText();
		
		if(avgViews == true )
		{
			System.out.println("Data loaded successfully!");
			System.out.println(avgViewsText+" : "+views);
		}
		else
		{
			System.out.println("Data loading failed!");
		}
		
		//Average Comments
		System.out.println("**********Average Comments**********");
		boolean avgComments = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[4]/div[1]/div[2]/div/div[1]/h3")).isDisplayed();
		String comments = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[4]/div[1]/div[2]/div/div[1]/h3")).getText();
		String avgCommentsText = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[4]/div[1]/div[2]/div/div[2]/span")).getText();
		
		if(avgComments == true )
		{
			System.out.println("Data loaded successfully!");
			System.out.println(avgCommentsText+" : "+comments);
		}
		else
		{
			System.out.println("Data loading failed!");
		}
		
		//Engagement Rate
		System.out.println("**********Engagement Rate**********");
		boolean engRate = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[4]/div[2]/div[1]/div/div[1]/h3")).isDisplayed();
		String engageRate = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[4]/div[2]/div[1]/div/div[1]/h3")).getText();
		String engageRateText = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[4]/div[2]/div[1]/div/div[2]/span")).getText();
		
		if(engRate == true )
		{
			System.out.println("Data loaded successfully!");
			System.out.println(engageRateText+" : "+engageRate);
		}
		else
		{
			System.out.println("Data loading failed!");
		}
		
		//Average Likes
		System.out.println("**********Average Likes**********");
		boolean avgLikes = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[4]/div[1]/div[3]/div/div[1]/h3")).isDisplayed();
		String likes = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[4]/div[1]/div[3]/div/div[1]/h3")).getText();
		String avgLikesText = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[4]/div[1]/div[3]/div/div[2]/span")).getText();
			
		if(avgLikes == true )
		{
			System.out.println("Data loaded successfully!");
			System.out.println(avgLikesText+" : "+likes);
		}
		else
		{
			System.out.println("Data loading failed!");
		}
		
		//Average Reels Plays
		System.out.println("**********Average Reels Plays**********");
		boolean avgPlays = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[4]/div[1]/div[4]/div/div[1]/h3")).isDisplayed();
		String reelsPlays = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[4]/div[1]/div[4]/div/div[1]/h3")).getText();
		String avgPlaysText = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[4]/div[1]/div[4]/div/div[2]/span")).getText();
					
		if(avgPlays == true )
		{
			System.out.println("Data loaded successfully!");
			System.out.println(avgPlaysText+" : "+reelsPlays);
		}
		else
		{
			System.out.println("Data loading failed!");
		}
		
		//Paid Post Performance
		System.out.println("**********Paid Post Performance**********");
		boolean paidPost = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[4]/div[2]/div[2]/div/div[1]/h3")).isDisplayed();
		String ppp = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[4]/div[2]/div[2]/div/div[1]/h3")).getText();
		String postText = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[4]/div[2]/div[2]/div/div[2]/span")).getText();
					
		if(paidPost == true )
		{
			System.out.println("Data loaded successfully!");
			System.out.println(postText+" : "+ppp);
		}
		else
		{
			System.out.println("Data loading failed!");
		}
		
		//Average Interactions
		System.out.println("**********Average Interactions**********");
		boolean avgIntr = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[5]/div[1]/div/div[1]/h3")).isDisplayed();
		String intr = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[5]/div[1]/div/div[1]/h3")).getText();
		String avgIntrText = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[5]/div[1]/div/div[2]/span")).getText();
							
		if(avgIntr == true )
		{
			System.out.println("Data loaded successfully!");
			System.out.println(avgIntrText+" : "+intr);
		}
		else
		{
			System.out.println("Data loading failed!");
		}
		
		//Sponsored Reel Views
		System.out.println("**********Sponsored Reel Views**********");
		boolean reelsViews = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[5]/div[2]/div/div[1]/h3")).isDisplayed();
		String sponsoredViews = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[5]/div[2]/div/div[1]/h3")).getText();
		String sponsoredViewsText = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[5]/div[2]/div/div[2]/span")).getText();
								
		if(reelsViews == true )
		{
			System.out.println("Data loaded successfully!");
			System.out.println(sponsoredViewsText+" : "+sponsoredViews);
		}
		else
		{
			System.out.println("Data loading failed!");
		}
		
		//TopicTensor
		System.out.println("**********TopicTensor**********");
		boolean tt = driver.findElement(By.xpath("//*[@id=\"TopicTensor_graph\"]")).isDisplayed();
		List<WebElement> topicTensor = driver.findElements(By.xpath("//*[@id=\"TopicTensor_graph\"]/span"));
		if(tt == true)
		{
			System.out.println("Data loaded successfully!");
			for (WebElement webElement : topicTensor)
			{
				if(webElement.getText().equals(topicTensor))
				{
					System.out.println(webElement);
				}
			}
		}
		else
		{
			System.out.println("Data loading failed!");
		}
		
		//Lookalikes
		System.out.println("**********Lookalikes**********");
		boolean lookAlikes = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[6]/div[2]/div/div[2]")).isDisplayed();
		if(lookAlikes == true)
		{
			System.out.println("Data loaded successfully!");
			List<WebElement> lookalikes = new ArrayList<>();
			for (WebElement webElement : lookalikes)
			{
				lookalikes = driver.findElements(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[6]/div[2]/div/div[2]/div["+rowcounter+"]/div[2]"));
				if(webElement.getText().equals(lookalikes))
				{
					System.out.println(webElement);
				}
				rowcounter++;
			}
		}
		else
		{
			System.out.println("Data loading failed!");
		}
		
		//Followers
		System.out.println("**********Followers**********");
		boolean folgraph = driver.findElement(By.xpath("//*[@id=\"highcharts-obpg0ic-0\"]/svg/rect[1]")).isDisplayed();
		String followersGraph = driver.findElement(By.xpath("//*[@id=\"highcharts-obpg0ic-0\"]/svg/rect[1]")).getText();
		String followersGraphText = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[7]/div[1]/div/div[1]/span")).getText();
		if(folgraph == true )
		{
			System.out.println("Data loaded successfully!");
			System.out.println(followersGraphText+" : "+followersGraph);
		}
		else
		{
			System.out.println("Data loading failed!");
		}
		
		//Following
		System.out.println("**********Following**********");
		boolean folograph = driver.findElement(By.xpath("//*[@id=\"highcharts-obpg0ic-12\"]/svg/rect[1]")).isDisplayed();
		String followingGraph = driver.findElement(By.xpath("//*[@id=\"highcharts-obpg0ic-12\"]/svg/rect[1]")).getText();
		String followingGraphText = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[7]/div[2]/div/div[1]/span")).getText();
		if(folograph == true )
		{
			System.out.println("Data loaded successfully!");
			System.out.println(followingGraphText+" : "+followingGraph);
		}
		else
		{
			System.out.println("Data loading failed!");
		}
		
		//Likes
		System.out.println("**********Likes**********");
		boolean likegraph = driver.findElement(By.xpath("//*[@id=\"highcharts-obpg0ic-24\"]/svg/rect[1]")).isDisplayed();
		String likesGraph = driver.findElement(By.xpath("//*[@id=\"highcharts-obpg0ic-24\"]/svg/rect[1]")).getText();
		String likesText = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[7]/div[3]/div/div[1]/span")).getText();
		if(likegraph == true )
		{
			System.out.println("Data loaded successfully!");
			System.out.println(likesText+" : "+likesGraph);
		}
		else
		{
			System.out.println("Data loading failed!");
		}
		
		//Engagement spread for last posts
		System.out.println("**********Engagement spread for last posts**********");
		boolean ergraph = driver.findElement(By.xpath("//*[@id=\"highcharts-obpg0ic-36\"]/svg/rect[1]")).isDisplayed();
		String engageGraph = driver.findElement(By.xpath("//*[@id=\"highcharts-obpg0ic-36\"]/svg/rect[1]")).getText();
		String engageGraphText = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[8]/div/div[1]/span")).getText();
		if(ergraph == true )
		{
			System.out.println("Data loaded successfully!");
			System.out.println("Engagement spread Graph : "+engageGraph);
		}
		else
		{
			System.out.println("Data loading failed!");
		}
		
		//Popular#
		System.out.println("**********Popular#**********");
		boolean popHash = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[9]/div[1]/div/div[2]")).isDisplayed();
		if(popHash == true)
		{
			System.out.println("Data loaded successfully!");
			rowcounter = 1;
			List<WebElement> popularHashtag = new ArrayList<>();
			for (WebElement webElement : popularHashtag)
			{
				popularHashtag = driver.findElements(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[9]/div[1]/div/div[2]/span[\"+rowcounter+\"]"));
				if(webElement.getText().equals(popularHashtag))
				{
					System.out.println(webElement);
				}
				rowcounter++;
			}
		}
		else
		{
			System.out.println("Data loading failed!");
		}
		
		//Popular@
		System.out.println("**********Popular@**********");
		boolean popMention = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[9]/div[2]/div")).isDisplayed();
		if(popMention == true)
		{
			System.out.println("Data loaded successfully!");
			rowcounter = 1;
			List<WebElement> popularMention = new ArrayList<>();
			for (WebElement webElement : popularMention)
			{
				popularMention = driver.findElements(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[9]/div[2]/div/div[2]/span[\"+rowcount+\"]"));
				if(webElement.getText().equals(popularMention))
				{
					System.out.println(webElement);
				}
				rowcounter++;
			}
		}
		else
		{
			System.out.println("Data loading failed!");
		}
		
		//Influencer Interests
		System.out.println("**********Influencer Interests**********");
		boolean inflIntr = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[9]/div[3]/div/div[2]")).isDisplayed();
		if(inflIntr == true)
		{
			System.out.println("Data loaded successfully!");
			rowcounter = 1;
			List<WebElement> influencerInterests = new ArrayList<>();
			for (WebElement webElement : influencerInterests)
			{
				influencerInterests = driver.findElements(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[9]/div[3]/div/div[2]/ul/li[\"+rowcount+\"]"));
				if(webElement.getText().equals(influencerInterests))
				{
					System.out.println(webElement);
				}
				rowcounter++;
			}
		}
		else
		{
			System.out.println("Data loading failed!");
		}
		
		//Audience Data
		
		//Audience Type
		System.out.println("**********Audience Type**********");
		boolean audType = driver.findElement(By.xpath("//*[@id=\"highcharts-obpg0ic-59\"]/svg/rect[1]")).isDisplayed();
		String audienceType = driver.findElement(By.xpath("//*[@id=\"highcharts-obpg0ic-59\"]/svg/rect[1]")).getText();
		String audTypeText = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[12]/div/div[1]/span")).getText();
		if(audType == true)
		{
			System.out.println("Data loaded successfully!");
			System.out.println(audTypeText+" : "+audienceType);
		}
		else
		{
			System.out.println("Data loading failed!");
		}
		
		//Gender Split
		System.out.println("**********Gender Split**********");
		boolean gensplit = driver.findElement(By.xpath("//*[@id=\"highcharts-obpg0ic-65\"]/svg/rect[1]")).isDisplayed();
		String genderSplit = driver.findElement(By.xpath("//*[@id=\"highcharts-obpg0ic-65\"]/svg/rect[1]")).getText();
		String genderSplitType = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[13]/div[1]/div/div[1]/span")).getText();
		if(gensplit == true)
		{
			System.out.println("Data loaded successfully!");
			System.out.println(genderSplitType+" : "+genderSplit);
		}
		else
		{
			System.out.println("Data loading failed!");
		}
		
		//Age and Gender Split
		System.out.println("**********Age and Gender Split**********");
		boolean ageSplit = driver.findElement(By.xpath("//*[@id=\"highcharts-obpg0ic-69\"]/svg/rect[1]")).isDisplayed();
		String ageGenderSplit = driver.findElement(By.xpath("//*[@id=\"highcharts-obpg0ic-69\"]/svg/rect[1]")).getText();
		String ageGenderText = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[13]/div[2]/div/div[1]/span")).getText();
		if(ageSplit == true )
		{
			System.out.println("Data loaded successfully!");
			System.out.println(ageGenderText+" : "+ageGenderSplit);
		}
		else
		{
			System.out.println("Data loading failed!");
		}
		
		/*js.executeScript("window.scrollBy(0,650)");

		Thread.sleep(2000);
		((JavascriptExecutor) driver) .executeScript("window.scrollTo(0, -document.body.scrollHeight)");*/

		//Location by City
		System.out.println("**********Location by City**********");
		boolean  locCity = driver.findElement(By.xpath("//*[@id=\"highcharts-obpg0ic-84\"]/svg/rect[1]")).isDisplayed();
		String locationCity = driver.findElement(By.xpath("//*[@id=\"highcharts-obpg0ic-84\"]/svg/rect[1]")).getText();
		String locationCityText = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[14]/div/div[1]/span")).getText();
		if (locCity == true )
		{
			System.out.println("Data loaded successfully!");
			System.out.println(locationCityText+" : "+locationCity);
		}
		else
		{
			System.out.println("Data loading failed!");
		}
		
		//Location by Country
		System.out.println("**********Location by Country**********");
		boolean  locCountry = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[15]/div[1]/div/div[2]")).isDisplayed();
		String locationCountryText = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[15]/div[1]/div/div[1]/span")).getText();
		if (locCountry == true )
		{
			System.out.println("Data loaded successfully!");
			System.out.println(locationCountryText);
			List<WebElement> locationCountry = new ArrayList<>();
			locationCountry.addAll(driver.findElements(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[15]/div[1]/div/div[2]/div/div[1]/div[1]")));
			
			List<WebElement> locValues = new ArrayList<>();
			locValues.addAll(driver.findElements(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[15]/div[1]/div/div[2]/div/div[1]/div[2]")));
			for(int i = 1; i <= locationCountry.size(); i++)
			{
				System.out.println(locationCountry.get(i)+" = "+locValues.get(i));
			}
		}
		else
		{
			System.out.println("Data loading failed!");
		}
		
		//Ethnicity
		System.out.println("**********Ethnicity**********");
		boolean  ethnic = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[15]/div[2]/div/div[2]")).isDisplayed();
		String ethnicityText = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[15]/div[2]/div/div[1]/span")).getText();
		if(ethnic == true )
		{
			System.out.println("Data loaded successfully!");
			System.out.println(ethnicityText);
			List<WebElement> ethnicity = new ArrayList<>();
			ethnicity.addAll(driver.findElements(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[15]/div[2]/div/div[2]/div/div[1]/div[1]")));
			
			List<WebElement> ethnicityValues = new ArrayList<>();
			ethnicityValues.addAll(driver.findElements(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[15]/div[2]/div/div[2]/div/div[1]/div[2]")));
			for(int i = 1; i <= ethnicity.size(); i++)
			{
				System.out.println(ethnicity.get(i)+" = "+ethnicityValues.get(i));
			}
		}
		else
		{
			System.out.println("Data loading failed!");
		}
		
		//Language
		System.out.println("**********Language**********");
		boolean lang = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[15]/div[3]/div/div[2]")).isDisplayed();
		String languageText = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[15]/div[3]/div/div[1]/span")).getText();
		if(lang == true )
		{
			System.out.println("Data loaded successfully!");
			System.out.println(languageText);
			List<WebElement> language = new ArrayList<>();
			language.addAll(driver.findElements(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[15]/div[3]/div/div[2]/div/div[1]/div[1]")));
			
			List<WebElement> languageValues = new ArrayList<>();
			languageValues.addAll(driver.findElements(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[15]/div[3]/div/div[2]/div/div[1]/div[2]")));
			for(int i = 1; i <= language.size(); i++)
			{
				System.out.println(language.get(i)+" = "+languageValues.get(i));
			}
		}
		else
		{
			System.out.println("Data loading failed!");
		}
		
		//Audience Brand Affinity
		System.out.println("**********Audience Brand Affinity**********");
		boolean brdAff = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[16]/div[1]/div/div[2]/div[1]")).isDisplayed();
		String brdAffinityText = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[16]/div[1]/div/div[1]/span")).getText();
		if(brdAff == true )
		{
			System.out.println("Data loaded successfully!");
			System.out.println(brdAffinityText);
			List<WebElement> brandAffinity = new ArrayList<>();
			brandAffinity.addAll(driver.findElements(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[16]/div[1]/div/div[2]/div[1]/div/div[1]")));
			
			List<WebElement> brandAffinityValues = new ArrayList<>();
			brandAffinityValues.addAll(driver.findElements(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[16]/div[1]/div/div[2]/div[1]/div/div[2]")));
			for(int i = 1; i <= brandAffinity.size(); i++)
			{
				System.out.println(brandAffinity.get(i)+" = "+brandAffinityValues.get(i));
			}
		}
		else
		{
			System.out.println("Data loading failed!");
		}
		
		//Audience Interests Affinity
		System.out.println("**********Audience Interests Affinity**********");
		boolean intrAff = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[16]/div[2]/div/div[2]")).isDisplayed();
		String intrAffinityText = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[16]/div[2]/div/div[1]")).getText();
		if(intrAff == true )
		{
			System.out.println("Data loaded successfully!");
			System.out.println(intrAffinityText);
			List<WebElement> interestsAffinity = new ArrayList<>();
			interestsAffinity.addAll(driver.findElements(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[16]/div[2]/div/div[2]/div/div[1]")));
			
			List<WebElement> interestsValues = new ArrayList<>();
			interestsValues.addAll(driver.findElements(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[16]/div[2]/div/div[2]/div/div[2]")));
			for(int i = 1; i <= interestsAffinity.size(); i++)
			{
				System.out.println(interestsAffinity.get(i)+" = "+interestsValues.get(i));
			}
		}
		else
		{
			System.out.println("Data loading failed!");
		}
		
		//Audience looklikes
		System.out.println("**********Audience looklikes**********");
		boolean audLook = driver.findElement(By.xpath("//*[@id=\"audience_lookalikes_list\"]")).isDisplayed();
		String audLookLikes = driver.findElement(By.xpath("//*[@id=\"audience_lookalikes_list\"]")).getText();
		String audLookText = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[16]/div[3]/div/div[1]/span")).getText();
		if(audLook == true )
		{
			rowcounter = 1;
			List<WebElement> audienceLooklikes = new ArrayList<>();
			for (WebElement webElement : audienceLooklikes)
			{
				audienceLooklikes = driver.findElements(By.xpath("//*[@id=\"audience_lookalikes_list\"]/div[\"+rowcount+\"]"));
				if(webElement.getText().equals(audienceLooklikes))
				{
					System.out.println(webElement);
				}
				rowcounter++;
			}
		}
		else
		{
			System.out.println("Data loading failed!");
		}
		
		driver.findElement(By.xpath("/html/body/div[3]/div[1]/div")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("/html/body/div[3]/div[3]/div[4]/a")).click();
		Thread.sleep(2000);
		driver.close();
		Thread.sleep(5000);
		driver.switchTo().window(tabs2.get(0));
	}
	
	public void Advance_filter_Testing_Instagram() throws InterruptedException
	{
		influencer_suite.click();
		curated_list.click();
		add.click();
		//Advance_filter.click();
		Thread.sleep(2000);
		
		//Select Interest
		driver.findElement(By.xpath("//*[@id=\"ig_filters\"]/div/div[1]/div[1]/span")).click();
		driver.findElement(By.xpath("//*[@id=\"ig_filters\"]/div/div[1]/div[1]/div/div/div/div/div/button/span[1]")).click();
		Random random = new Random();
        int randomNumber = random.nextInt(29);
		driver.findElement(By.xpath("//*[@id=\"ig_filters\"]/div/div[1]/div[1]/div/div/div/div/div/div/ul/li["+randomNumber+"]/a")).click();
		closePopUp.click();
		
		//Select Audience location
		driver.findElement(By.xpath("//*[@id=\"ig_filters\"]/div/div[1]/div[2]/span")).click();
		driver.findElement(By.xpath("//*[@id=\"ig_filters\"]/div/div[1]/div[2]/div/div/div/div[1]/div[2]/button")).click();
		driver.findElement(By.xpath("//*[@id=\"ig_filters\"]/div/div[1]/div[2]/div/div/div/div[1]/div[2]/div/div/input")).sendKeys("mumbai");
		Thread.sleep(2000);
		driver.findElement(By.linkText("Mumbai")).click();

		//Select Influencer location
		driver.findElement(By.xpath("//*[@id=\"ig_filters\"]/div/div[1]/div[2]/div/div/div/div[2]/div[2]/button")).click();
		driver.findElement(By.xpath("//*[@id=\"ig_filters\"]/div/div[1]/div[2]/div/div/div/div[2]/div[2]/div/div/input")).sendKeys("mumbai");
		Thread.sleep(2000);
		driver.findElement(By.linkText("Mumbai")).click();
		driver.findElement(By.xpath("//*[@id=\"ig_filters\"]/div/div[1]/div[2]/div/div/div/div[2]/span")).click();
		
		//Audience Gender
		driver.findElement(By.xpath("//*[@id=\"ig_filters\"]/div/div[1]/div[3]/span")).click();
		randomNumber = random.nextInt(3) + 1;
		driver.findElement(By.xpath("//*[@id=\"ig_filters\"]/div/div[1]/div[3]/div/div/div/div[1]/div["+randomNumber+"]")).click();
		
		//Influencer Gender
		randomNumber = random.nextInt(3) + 1;
		driver.findElement(By.xpath("//*[@id=\"ig_filters\"]/div/div[1]/div[3]/div/div/div/div[2]/div["+randomNumber+"]")).click();
		closePopUp.click();

		//Audience Age
		driver.findElement(By.xpath("//*[@id=\"ig_filters\"]/div/div[1]/div[4]/span")).click();
		driver.findElement(By.xpath("//*[@id=\"ig_filters\"]/div/div[1]/div[4]/div/div/div/div/div[1]/div[2]/div/div/button")).click();
		randomNumber = random.nextInt(7);
		driver.findElement(By.xpath("//*[@id=\"ig_filters\"]/div/div[1]/div[4]/div/div/div/div/div[1]/div[2]/div/div/div/ul/li["+randomNumber+"]/a")).click();
		
		//Influencer Age
		driver.findElement(By.xpath("//*[@id=\"ig_filters\"]/div/div[1]/div[4]/div/div/div/div/div[2]/div[2]/div/div/button")).click();
		randomNumber = random.nextInt(7);
		driver.findElement(By.xpath("//*[@id=\"ig_filters\"]/div/div[1]/div[4]/div/div/div/div/div[2]/div[2]/div/div/div/ul/li["+randomNumber+"]/a")).click();
		closePopUp.click();
		
		//Followers Range
		driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[3]/div/div[2]/div/div/div/div[2]/div/div[1]/div[5]/span")).click();
		
		//from value
		driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[3]/div/div[2]/div/div/div/div[2]/div/div[1]/div[5]/div/div/div/div[2]/div[1]/div/button")).click();
		int from = random.nextInt(8)+1;
		driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[3]/div/div[2]/div/div/div/div[2]/div/div[1]/div[5]/div/div/div/div[2]/div[1]/div/div/ul/li["+from+"]/a")).click();
		
		//to value
		driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[3]/div/div[2]/div/div/div/div[2]/div/div[1]/div[5]/div/div/div/div[2]/div[2]/div/button")).click();
		int to = random.nextInt(8 - from + 1) + from;
		driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[3]/div/div[2]/div/div/div/div[2]/div/div[1]/div[5]/div/div/div/div[2]/div[2]/div/div/ul/li[\"+to+\"]/a")).click();
		closePopUp.click();
		
		//Partnership
		driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[3]/div/div[2]/div/div/div/div[2]/div/div[1]/div[6]/span")).click();
		driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[3]/div/div[2]/div/div/div/div[2]/div/div[1]/div[6]/div/div/div/div/div/button")).click();
		List<WebElement> count = driver.findElements(By.xpath("/html/body/div[5]/div[2]/div[3]/div/div[2]/div/div/div/div[2]/div/div[1]/div[6]/div/div/div/div/div/div/ul/li/a"));
		int range = count.size();
		randomNumber = random.nextInt(range);
		driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[3]/div/div[2]/div/div/div/div[2]/div/div[1]/div[6]/div/div/div/div/div/div/ul/li["+randomNumber+"]/a")).click();
		driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[3]/div/div[2]/div/div/div/div[2]/div/div[1]/div[6]/div/div/div/span")).click();

		//Bio
		driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[3]/div/div[2]/div/div/div/div[2]/div/div[1]/div[7]/span")).click();
		driver.findElement(By.xpath("//*[@id=\"bio_search_list\"]")).sendKeys("beauty");
		driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[3]/div/div[2]/div/div/div/div[2]/div/div[1]/div[7]/div/div/div/div/div/span")).click();
		
		//Engagements
		driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[3]/div/div[2]/div/div/div/div[2]/div/div[1]/div[8]/span")).click();
		
		//from value
		driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[3]/div/div[2]/div/div/div/div[2]/div/div[1]/div[8]/div/div/div/div[2]/div[1]/div/button")).click();
		from = random.nextInt(9)+1;
		driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[3]/div/div[2]/div/div/div/div[2]/div/div[1]/div[8]/div/div/div/div[2]/div[1]/div/div/ul/li["+from+"]/a")).click();
		
		//to value
		driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[3]/div/div[2]/div/div/div/div[2]/div/div[1]/div[5]/div/div/div/div[2]/div[2]/div/button")).click();
		to = random.nextInt(9 - from + 1) + from;
		driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[3]/div/div[2]/div/div/div/div[2]/div/div[1]/div[5]/div/div/div/div[2]/div[2]/div/div/ul/li[\"+to+\"]/a")).click();
		driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[3]/div/div[2]/div/div/div/div[2]/div/div[1]/div[8]/div/div/div/span")).click();
		
		String ig_list= driver.findElement(By.xpath("//*[@id=\"ig_list\"]/div")).getText();
		System.out.println("Result will be="+ig_list);
		
		//driver.findElement(By.xpath("//*[@id=\"ig_filters\"]/div[1]/div[2]/div[4]/div")).isDisplayed();
		WebElement slider = driver.findElement(By.xpath("//*[@id=\"ig_filters\"]/div[1]/div[2]/div[4]/div/span/span[3]"));
		Actions action =new Actions(driver);
	}
}
