package Testcases_Reports;
import java.awt.AWTException;
import java.awt.HeadlessException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import org.openqa.selenium.By;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import TestBase.Base_app;
import Utils.Testutils;
import modules.Reports;

public class Testcases_Create_Reports_Influencer_Campaign_All_Platform extends Base_app
{
	modules.Reports reps;
	Testutils testutils;

	public Testcases_Create_Reports_Influencer_Campaign_All_Platform()
	{
		super();
	}

	@BeforeMethod
	public void setup() throws IOException, AWTException, InterruptedException
	{
		Base_app.initialization();
		reps = new Reports();
	}

	//Influencer Campaign
	@Test(priority=1)
	public void Testcase_Create_Influencer_Campaign_Reports_Instagram_single_hashtag() throws InterruptedException
	{
		reps.Reports_Create_Influencer_Campaign_Instagram();
	}

	@Test(priority=2)
	public void Testcase_Create_Influencer_Campaign_Reports_Youtube() throws InterruptedException
	{
		reps.Reports_Create_Influencer_Campaign_Youtube();
	}

	@Test(priority=3)
	public void Testcase_Create_Influencer_Campaign_Reports_Twitter() throws InterruptedException
	{
		reps.Reports_Create_Influencer_Campaign_Twitter();
	}

	@Test(priority=4)
	public void Testcase_Create_Influencer_Campaign_Reports_IG_YT_TW() throws InterruptedException
	{
		reps.Reports_Create_Influencer_Campaign_Instagram_and_Youtube_and_Twitter();
	}

	@Test(priority=5)
	public void Testcase_Create_Influencer_Campaign_Reports_Instagram_and_Youtube() throws InterruptedException
	{
		reps.Reports_Create_Influencer_Campaign_Instagram_and_Youtube();
	}

	@Test(priority=6)
	public void Testcases_Reports_StoryForm_Instagram() throws InterruptedException, HeadlessException, UnsupportedFlavorException, IOException
	{
		reps.Report_Instagram_StoryForm_check();
	}

	@AfterMethod
	public void teardown() throws InterruptedException
	{
		Thread.sleep(2000);
		driver.findElement(By.xpath("/html/body/div[3]/div[1]/div")).isEnabled();
		Thread.sleep(2000);
		driver.findElement(By.xpath("/html/body/div[3]/div[1]/div")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("/html/body/div[3]/div[3]/div[4]/a")).click();
		driver.close();
	}
}