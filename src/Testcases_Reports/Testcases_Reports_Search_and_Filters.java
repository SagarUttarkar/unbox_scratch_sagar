package Testcases_Reports;
import java.awt.AWTException;
import java.io.IOException;

import org.openqa.selenium.By;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import TestBase.Base_app;
import Utils.Testutils;
import modules.Reports;

public class Testcases_Reports_Search_and_Filters extends Base_app
{
	modules.Reports reps;
	Testutils testutils;

	public Testcases_Reports_Search_and_Filters()
	{
		super();
	}

	@BeforeMethod
	public void setup() throws IOException, AWTException, InterruptedException
	{
		Base_app.initialization();
		reps = new Reports();
	}
	
	@Test(priority=11)
	public void Testcases_all_Reports() throws InterruptedException
	{
		reps.All_Reports();
	}

	@Test(priority=12)
	public void Testcases_all_Reports_search() throws InterruptedException
	{
		reps.All_Reports_Search();
	}

	@Test(priority=13)
	public void Testcases_Instagram_Report_filter() throws InterruptedException
	{
		reps.Instagram_Report_Filter();
	}
	
	@Test(priority=14)
	public void Testcases_Facebook_Report_filter() throws InterruptedException
	{
		reps.Facebook_Report_Filter();
	}
	
	@Test(priority=15)
	public void Testcases_Twitter_Report_filter() throws InterruptedException
	{
		reps.Twitter_Report_Filter();
	}
	
	@Test(priority=16)
	public void Testcases_Youtube_Report_filter() throws InterruptedException
	{
		reps.Youtube_Report_Filter();
	}

	@AfterMethod
	public void teardown() throws InterruptedException
	{
		Thread.sleep(2000);
		driver.findElement(By.xpath("/html/body/div[3]/div[1]/div")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("/html/body/div[3]/div[3]/div[4]/a")).click();
		driver.close();
	}
}