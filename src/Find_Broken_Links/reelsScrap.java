package Find_Broken_Links;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class reelsScrap {
	
	WebDriver driver;
	
	   @Test
	   (priority=0,dataProvider="brands")
		public  void test(String l1) throws InterruptedException {
			System.setProperty("webdriver.chrome.driver","Drivers//chromedriver_v94.exe");
			 driver =new ChromeDriver();
			ChromeOptions options = new ChromeOptions();
			options.setPageLoadStrategy(PageLoadStrategy.NONE);
			 WebDriverWait wait = new WebDriverWait(driver, 1200);
			System.setProperty(ChromeDriverService.CHROME_DRIVER_SILENT_OUTPUT_PROPERTY,"true");
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
			driver.manage().window().maximize();
			driver.manage().deleteAllCookies();
			driver.get("https://instavideosave.net/");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@placeholder='Paste Reels Link ...']")));
			driver.findElement(By.xpath("//input[@placeholder='Paste Reels Link ...']")).sendKeys(l1);
			Thread.sleep(10000);
			driver.findElement(By.xpath("//button[@type='submit']")).click();
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollBy(0,450)");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("body > div:nth-child(1) > main:nth-child(4) > div:nth-child(1) > div:nth-child(1) > div:nth-child(7) > div:nth-child(1) > div:nth-child(2) > button:nth-child(1)")));
			driver.findElement(By.cssSelector("body > div:nth-child(1) > main:nth-child(4) > div:nth-child(1) > div:nth-child(1) > div:nth-child(7) > div:nth-child(1) > div:nth-child(2) > button:nth-child(1)")).click();
			Thread.sleep(8000);
			
}
	   @AfterMethod
	   public void teardown() {
		   
		   driver.close();
		   
	   }
	   
	   @DataProvider(name="brands")
	   public Object[][] passdata(){
	    	//here is first is row nd then col

	    	Object[][] data= new Object[8][1];

	    	data[0][0]="https://www.instagram.com/reel/CZEjUZsBjOW/";
	    	data[1][0]="https://www.instagram.com/reel/CZZLNY0q3QC/";
	    	data[2][0]="https://www.instagram.com/reel/CZEl5p6jyYm/";
	    	data[3][0]="https://www.instagram.com/reel/CZOn4jNIC7q/";
	    	data[4][0]="https://www.instagram.com/reel/CZJOcgTFcYO/";
	    	data[5][0]="https://www.instagram.com/reel/CZEAzsPAUDw/";
	    	data[6][0]="https://www.instagram.com/reel/CZCRJ6Rh_Bh/";
	    	data[7][0]="https://www.instagram.com/reel/CZHOQ1Fo0zL/";
	    	
return data;
	   }
}