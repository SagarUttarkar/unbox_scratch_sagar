package Testcases_Reports;

import java.awt.AWTException;
import java.io.IOException;
import org.openqa.selenium.By;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import TestBase.base_app1;
import Utils.Testutils;
import modules.Reports_Influencer_Tracking_and_Brand_Safety;

public class Testcases_Create_Report_Infltracking_Brand_Safety extends base_app1
{
	modules.Reports_Influencer_Tracking_and_Brand_Safety reps_influ;
	Testutils testutils;

	public Testcases_Create_Report_Infltracking_Brand_Safety()
	{
		super();
	}

	@BeforeMethod
	public void setup() throws IOException, AWTException, InterruptedException
	{
		base_app1.initialization();
		reps_influ = new Reports_Influencer_Tracking_and_Brand_Safety();
	}

	@Test(priority=1)
	public void Testcase_Create_Report_Brand_Safety() throws InterruptedException
	{	
		reps_influ.Testcases_Brand_Safety_export();
		System.out.println("Succsessfully generated Brand Safety Report");
	}

	@Test(priority=2)
	public void Testcases_Brand_Safety() throws InterruptedException
	{
		reps_influ.Brand_Safety();
	}

	/*@Test(priority=3)
	public void Testcases_Export_Reports_influencer_tracking() throws InterruptedException
	{
		reps_influ.Testcases_Influencer_tracking_camping();
	}*/

	@AfterMethod
	public void teardown() throws InterruptedException
	{
		Thread.sleep(2000);
		driver.findElement(By.xpath("/html/body/div[3]/div[1]/div")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("/html/body/div[3]/div[3]/div[4]/a")).click();
		driver.close();
	}
}