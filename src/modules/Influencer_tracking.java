package modules;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import TestBase.base_app1;

public class Influencer_tracking extends base_app1
{
	WebElement influencertracking = driver.findElement(By.xpath("//img[contains(@src,'https://app.unboxsocial.com/public/images/sidebar-icons/infltracking.svg')]"));

	WebElement influencertracking_list = driver.findElement(By.xpath("//a[@href='https://app.unboxsocial.com/influencerTracking']"));

	WebElement addplus = driver.findElement(By.xpath("//i[@class='fa fa-plus add_acc_plus']"));
	
	WebElement searchicon = driver.findElement(By.xpath("//img[@class='list-search-icon search-anywhere-in-page']"));

	WebElement searchinput = driver.findElement(By.xpath("//input[@placeholder='Search by Name or Handle']"));

	WebElement backbtn = driver.findElement(By.xpath("//a[normalize-space()='Back']"));

	WebElement brand_safety_report = driver.findElement(By.xpath("//a[@href='https://app.unboxsocial.com/influencerTracking/brandSafety']"));
	
	WebElement viewButton = driver.findElement(By.xpath("//*[@id=\"ig_infl_list_table\"]/tr/td[6]/a"));

	public  Influencer_tracking()
	{
		PageFactory.initElements(driver, this);
	}
	JavascriptExecutor js = (JavascriptExecutor) driver;
	WebDriverWait wait = new WebDriverWait(driver, 1200);  

	public void Testcase_Influencer_tracking_IG_search_suggestion(String IG_handlename) throws InterruptedException
	{
		Thread.sleep(2000);
		influencertracking.click();
		Thread.sleep(2000);
		
		influencertracking_list.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//i[@class='fa fa-plus add_acc_plus']")));
		
		addplus.click();
		Thread.sleep(5000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("recommended-influencer")));
		searchinput.sendKeys(IG_handlename);

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='inner_content']")));
		driver.findElement(By.xpath("/html/body/div[5]/div[2]/div/div/div[1]/div/div/div[2]/div/div/div/div/div/div[2]/div[1]/div[3]/div/div/table/tbody/tr[1]/td[1]/div/img")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"track\"]")).click();
		Thread.sleep(5000);
		backbtn.click();
		Thread.sleep(2000);
		driver.navigate().refresh();
		searchicon.click();
		Thread.sleep(5000);
		searchinput.sendKeys(IG_handlename);
		Thread.sleep(5000);
		driver.findElement(By.xpath("/html/body/div[5]/div[2]/div/div/div[2]/div[2]/div/div/div/table/tbody/tr/td[6]/img")).click();
		Thread.sleep(2000);
		driver.findElement(By.id("delete-btn")).click();
		Thread.sleep(3000);
	}
	
	public void Testcase_IG_metrics_validation(String IG_handlename ) throws InterruptedException, AWTException
	{
		int rowcounter = 1;
		Thread.sleep(2000);
		influencertracking.click();
		Thread.sleep(2000);
		influencertracking_list.click();
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//i[@class='fa fa-plus add_acc_plus']")));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[5]/div[2]/div/div/div[2]/div[2]/div/div")));
		
		searchicon.click();
		searchinput.sendKeys(IG_handlename);
		Thread.sleep(5000);
		
		driver.findElement(By.xpath("//i[@class='fa fa-eye view_right']")).click();
		ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
		viewButton.click();
		Thread.sleep(2000);
		
		//Analytics tab
		System.out.println("Analytics Tab");
				
		//Followers
		System.out.println("**********Followers**********");
		boolean fol = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[3]/div[1]/div/div[1]/h3")).isDisplayed();
		String followers = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[3]/div[1]/div/div[1]/h3")).getText();
		String followersText = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[3]/div[1]/div/div[2]/span")).getText();
				
		if(fol == true )
		{
			System.out.println("Data loaded successfully!");
			System.out.println(followersText+" : "+followers);
		}
		else
		{
			System.out.println("Data loading failed!");
		}
			
		//Location
		System.out.println("**********Location**********");
		boolean loc = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[3]/div[2]/div/div[1]/h3/span")).isDisplayed();
		String location = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[3]/div[2]/div/div[1]/h3/span")).getText();
		String locationText = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[3]/div[2]/div/div[2]/span")).getText();
				
		if(loc == true )
		{
			System.out.println("Data loaded successfully!");
			System.out.println(locationText+" : "+location);
		}
		else
		{
			System.out.println("Data loading failed!");
		}
		
		//Average Views
		System.out.println("**********Average Views**********");
		boolean avgViews = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[4]/div[1]/div[1]/div/div[1]/h3")).isDisplayed();
		String views = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[4]/div[1]/div[1]/div/div[1]/h3")).getText();
		String avgViewsText = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[4]/div[1]/div[1]/div/div[2]/span")).getText();
				
		if(avgViews == true )
		{
			System.out.println("Data loaded successfully!");
			System.out.println(avgViewsText+" : "+views);
		}
		else
		{
			System.out.println("Data loading failed!");
		}
		
		//Average Comments
		System.out.println("**********Average Comments**********");
		boolean avgComments = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[4]/div[1]/div[2]/div/div[1]/h3")).isDisplayed();
		String comments = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[4]/div[1]/div[2]/div/div[1]/h3")).getText();
		String avgCommentsText = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[4]/div[1]/div[2]/div/div[2]/span")).getText();
				
		if(avgComments == true )
		{
			System.out.println("Data loaded successfully!");
			System.out.println(avgCommentsText+" : "+comments);
		}
		else
		{
			System.out.println("Data loading failed!");
		}
				
		//Engagement Rate
		System.out.println("**********Engagement Rate**********");
		boolean engRate = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[4]/div[2]/div[1]/div/div[1]/h3")).isDisplayed();
		String engageRate = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[4]/div[2]/div[1]/div/div[1]/h3")).getText();
		String engageRateText = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[4]/div[2]/div[1]/div/div[2]/span")).getText();
			
		if(engRate == true )
		{
			System.out.println("Data loaded successfully!");
			System.out.println(engageRateText+" : "+engageRate);
		}
		else
		{
			System.out.println("Data loading failed!");
		}
		
		//Average Likes
		System.out.println("**********Average Likes**********");
		boolean avgLikes = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[4]/div[1]/div[3]/div/div[1]/h3")).isDisplayed();
		String likes = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[4]/div[1]/div[3]/div/div[1]/h3")).getText();
		String avgLikesText = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[4]/div[1]/div[3]/div/div[2]/span")).getText();
					
		if(avgLikes == true )
		{
			System.out.println("Data loaded successfully!");
			System.out.println(avgLikesText+" : "+likes);
		}
		else
		{
			System.out.println("Data loading failed!");
		}
				
		//Average Reels Plays
		System.out.println("**********Average Reels Plays**********");
		boolean avgPlays = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[4]/div[1]/div[4]/div/div[1]/h3")).isDisplayed();
		String reelsPlays = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[4]/div[1]/div[4]/div/div[1]/h3")).getText();
		String avgPlaysText = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[4]/div[1]/div[4]/div/div[2]/span")).getText();
					
		if(avgPlays == true )
		{
			System.out.println("Data loaded successfully!");
			System.out.println(avgPlaysText+" : "+reelsPlays);
		}
		else
		{
			System.out.println("Data loading failed!");
		}
				
		//Paid Post Performance
		System.out.println("**********Paid Post Performance**********");
		boolean paidPost = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[4]/div[2]/div[2]/div/div[1]/h3")).isDisplayed();
		String ppp = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[4]/div[2]/div[2]/div/div[1]/h3")).getText();
		String postText = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[4]/div[2]/div[2]/div/div[2]/span")).getText();
						
		if(paidPost == true )
		{
			System.out.println("Data loaded successfully!");
			System.out.println(postText+" : "+ppp);
		}
		else
		{
			System.out.println("Data loading failed!");
		}
				
		//Average Interactions
		System.out.println("**********Average Interactions**********");
		boolean avgIntr = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[5]/div[1]/div/div[1]/h3")).isDisplayed();
		String intr = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[5]/div[1]/div/div[1]/h3")).getText();
		String avgIntrText = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[5]/div[1]/div/div[2]/span")).getText();
									
		if(avgIntr == true )
		{
			System.out.println("Data loaded successfully!");
			System.out.println(avgIntrText+" : "+intr);
		}
		else
		{
			System.out.println("Data loading failed!");
		}
				
		//Sponsored Reel Views
		System.out.println("**********Sponsored Reel Views**********");
		boolean reelsViews = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[5]/div[2]/div/div[1]/h3")).isDisplayed();
		String sponsoredViews = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[5]/div[2]/div/div[1]/h3")).getText();
		String sponsoredViewsText = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[5]/div[2]/div/div[2]/span")).getText();
										
		if(reelsViews == true )
		{
			System.out.println("Data loaded successfully!");
			System.out.println(sponsoredViewsText+" : "+sponsoredViews);
		}
		else
		{
			System.out.println("Data loading failed!");
		}
				
		//TopicTensor
		System.out.println("**********TopicTensor**********");
		boolean tt = driver.findElement(By.xpath("//*[@id=\"TopicTensor_graph\"]")).isDisplayed();
		List<WebElement> topicTensor = driver.findElements(By.xpath("//*[@id=\"TopicTensor_graph\"]/span"));
		if(tt == true)
		{
			System.out.println("Data loaded successfully!");
			for (WebElement webElement : topicTensor)
			{
				if(webElement.getText().equals(topicTensor))
				{
					System.out.println(webElement);
				}
			}
		}
		else
		{
			System.out.println("Data loading failed!");
		}
				
		//Lookalikes
		System.out.println("**********Lookalikes**********");
		boolean lookAlikes = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[6]/div[2]/div/div[2]")).isDisplayed();
		if(lookAlikes == true)
		{
			System.out.println("Data loaded successfully!");
			List<WebElement> lookalikes = new ArrayList<>();
			for (WebElement webElement : lookalikes)
			{
				lookalikes = driver.findElements(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[6]/div[2]/div/div[2]/div["+rowcounter+"]/div[2]"));
				if(webElement.getText().equals(lookalikes))
				{
					System.out.println(webElement);
				}
				rowcounter++;
			}
		}
		else
		{
			System.out.println("Data loading failed!");
		}
		
		//Followers
		System.out.println("**********Followers**********");
		boolean folgraph = driver.findElement(By.xpath("//*[@id=\"highcharts-obpg0ic-0\"]/svg/rect[1]")).isDisplayed();
		String followersGraph = driver.findElement(By.xpath("//*[@id=\"highcharts-obpg0ic-0\"]/svg/rect[1]")).getText();
		String followersGraphText = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[7]/div[1]/div/div[1]/span")).getText();
		if(folgraph == true )
		{
			System.out.println("Data loaded successfully!");
			System.out.println(followersGraphText+" : "+followersGraph);
		}
		else
		{
			System.out.println("Data loading failed!");
		}
			
		//Following
		System.out.println("**********Following**********");
		boolean folograph = driver.findElement(By.xpath("//*[@id=\"highcharts-obpg0ic-12\"]/svg/rect[1]")).isDisplayed();
		String followingGraph = driver.findElement(By.xpath("//*[@id=\"highcharts-obpg0ic-12\"]/svg/rect[1]")).getText();
		String followingGraphText = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[7]/div[2]/div/div[1]/span")).getText();
		if(folograph == true )
		{
			System.out.println("Data loaded successfully!");
			System.out.println(followingGraphText+" : "+followingGraph);
		}
		else
		{
			System.out.println("Data loading failed!");
		}
				
		//Likes
		System.out.println("**********Likes**********");
		boolean likegraph = driver.findElement(By.xpath("//*[@id=\"highcharts-obpg0ic-24\"]/svg/rect[1]")).isDisplayed();
		String likesGraph = driver.findElement(By.xpath("//*[@id=\"highcharts-obpg0ic-24\"]/svg/rect[1]")).getText();
		String likesText = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[7]/div[3]/div/div[1]/span")).getText();
		if(likegraph == true )
		{
			System.out.println("Data loaded successfully!");
			System.out.println(likesText+" : "+likesGraph);
		}
		else
		{
			System.out.println("Data loading failed!");
		}
				
		//Engagement spread for last posts
		System.out.println("**********Engagement spread for last posts**********");
		boolean ergraph = driver.findElement(By.xpath("//*[@id=\"highcharts-obpg0ic-36\"]/svg/rect[1]")).isDisplayed();
		String engageGraph = driver.findElement(By.xpath("//*[@id=\"highcharts-obpg0ic-36\"]/svg/rect[1]")).getText();
		String engageGraphText = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[8]/div/div[1]/span")).getText();
		if(ergraph == true )
		{
			System.out.println("Data loaded successfully!");
			System.out.println("Engagement spread Graph : "+engageGraph);
		}
		else
		{
			System.out.println("Data loading failed!");
		}
				
		//Popular#
		System.out.println("**********Popular#**********");
		boolean popHash = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[9]/div[1]/div/div[2]")).isDisplayed();
		if(popHash == true)
		{
			System.out.println("Data loaded successfully!");
			rowcounter = 1;
			List<WebElement> popularHashtag = new ArrayList<>();
			for (WebElement webElement : popularHashtag)
			{
				popularHashtag = driver.findElements(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[9]/div[1]/div/div[2]/span[\"+rowcounter+\"]"));
				if(webElement.getText().equals(popularHashtag))
				{
					System.out.println(webElement);
				}
				rowcounter++;
			}
		}
		else
		{
			System.out.println("Data loading failed!");
		}
			
		//Popular@
		System.out.println("**********Popular@**********");
		boolean popMention = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[9]/div[2]/div")).isDisplayed();
		if(popMention == true)
		{
			System.out.println("Data loaded successfully!");
			rowcounter = 1;
			List<WebElement> popularMention = new ArrayList<>();
			for (WebElement webElement : popularMention)
			{
				popularMention = driver.findElements(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[9]/div[2]/div/div[2]/span[\"+rowcount+\"]"));
				if(webElement.getText().equals(popularMention))
				{
					System.out.println(webElement);
				}
				rowcounter++;
			}
		}
		else
		{
			System.out.println("Data loading failed!");
		}
				
		//Influencer Interests
		System.out.println("**********Influencer Interests**********");
		boolean inflIntr = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[9]/div[3]/div/div[2]")).isDisplayed();
		if(inflIntr == true)
		{
			System.out.println("Data loaded successfully!");
			rowcounter = 1;
			List<WebElement> influencerInterests = new ArrayList<>();
			for (WebElement webElement : influencerInterests)
			{
				influencerInterests = driver.findElements(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[9]/div[3]/div/div[2]/ul/li[\"+rowcount+\"]"));
				if(webElement.getText().equals(influencerInterests))
				{
					System.out.println(webElement);
				}
				rowcounter++;
			}
		}
		else
		{
			System.out.println("Data loading failed!");
		}
				
		//Audience Data
				
		//Audience Type
		System.out.println("**********Audience Type**********");
		boolean audType = driver.findElement(By.xpath("//*[@id=\"highcharts-obpg0ic-59\"]/svg/rect[1]")).isDisplayed();
		String audienceType = driver.findElement(By.xpath("//*[@id=\"highcharts-obpg0ic-59\"]/svg/rect[1]")).getText();
		String audTypeText = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[12]/div/div[1]/span")).getText();
		if(audType == true)
		{
			System.out.println("Data loaded successfully!");
			System.out.println(audTypeText+" : "+audienceType);
		}
		else
		{
			System.out.println("Data loading failed!");
		}
				
		//Gender Split
		System.out.println("**********Gender Split**********");
		boolean gensplit = driver.findElement(By.xpath("//*[@id=\"highcharts-obpg0ic-65\"]/svg/rect[1]")).isDisplayed();
		String genderSplit = driver.findElement(By.xpath("//*[@id=\"highcharts-obpg0ic-65\"]/svg/rect[1]")).getText();
		String genderSplitType = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[13]/div[1]/div/div[1]/span")).getText();
		if(gensplit == true)
		{
			System.out.println("Data loaded successfully!");
			System.out.println(genderSplitType+" : "+genderSplit);
		}
		else
		{
			System.out.println("Data loading failed!");
		}
				
		//Age and Gender Split
		System.out.println("**********Age and Gender Split**********");
		boolean ageSplit = driver.findElement(By.xpath("//*[@id=\"highcharts-obpg0ic-69\"]/svg/rect[1]")).isDisplayed();
		String ageGenderSplit = driver.findElement(By.xpath("//*[@id=\"highcharts-obpg0ic-69\"]/svg/rect[1]")).getText();
		String ageGenderText = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[13]/div[2]/div/div[1]/span")).getText();
		if(ageSplit == true )
		{
			System.out.println("Data loaded successfully!");
			System.out.println(ageGenderText+" : "+ageGenderSplit);
		}
		else
		{
			System.out.println("Data loading failed!");
		}
			
		/*js.executeScript("window.scrollBy(0,650)");
		Thread.sleep(2000);
		((JavascriptExecutor) driver) .executeScript("window.scrollTo(0, -document.body.scrollHeight)");*/

		//Location by City
		System.out.println("**********Location by City**********");
		boolean  locCity = driver.findElement(By.xpath("//*[@id=\"highcharts-obpg0ic-84\"]/svg/rect[1]")).isDisplayed();
		String locationCity = driver.findElement(By.xpath("//*[@id=\"highcharts-obpg0ic-84\"]/svg/rect[1]")).getText();
		String locationCityText = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[14]/div/div[1]/span")).getText();
		if (locCity == true )
		{
			System.out.println("Data loaded successfully!");
			System.out.println(locationCityText+" : "+locationCity);
		}
		else
		{
			System.out.println("Data loading failed!");
		}
			
		//Location by Country
		System.out.println("**********Location by Country**********");
		boolean  locCountry = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[15]/div[1]/div/div[2]")).isDisplayed();
		String locationCountryText = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[15]/div[1]/div/div[1]/span")).getText();
		if (locCountry == true )
		{
			System.out.println("Data loaded successfully!");
			System.out.println(locationCountryText);
			List<WebElement> locationCountry = new ArrayList<>();
			locationCountry.addAll(driver.findElements(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[15]/div[1]/div/div[2]/div/div[1]/div[1]")));
			
			List<WebElement> locValues = new ArrayList<>();
			locValues.addAll(driver.findElements(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[15]/div[1]/div/div[2]/div/div[1]/div[2]")));
			for(int i = 1; i <= locationCountry.size(); i++)
			{
				System.out.println(locationCountry.get(i)+" = "+locValues.get(i));
			}
		}
		else
		{
			System.out.println("Data loading failed!");
		}
				
		//Ethnicity
		System.out.println("**********Ethnicity**********");
		boolean  ethnic = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[15]/div[2]/div/div[2]")).isDisplayed();
		String ethnicityText = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[15]/div[2]/div/div[1]/span")).getText();
		if(ethnic == true )
		{
			System.out.println("Data loaded successfully!");
			System.out.println(ethnicityText);
			List<WebElement> ethnicity = new ArrayList<>();
			ethnicity.addAll(driver.findElements(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[15]/div[2]/div/div[2]/div/div[1]/div[1]")));
			
			List<WebElement> ethnicityValues = new ArrayList<>();
			ethnicityValues.addAll(driver.findElements(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[15]/div[2]/div/div[2]/div/div[1]/div[2]")));
			for(int i = 1; i <= ethnicity.size(); i++)
			{
				System.out.println(ethnicity.get(i)+" = "+ethnicityValues.get(i));
			}
		}
		else
		{
			System.out.println("Data loading failed!");
		}
				
		//Language
		System.out.println("**********Language**********");
		boolean lang = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[15]/div[3]/div/div[2]")).isDisplayed();
		String languageText = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[15]/div[3]/div/div[1]/span")).getText();
		if(lang == true )
		{
			System.out.println("Data loaded successfully!");
			System.out.println(languageText);
			List<WebElement> language = new ArrayList<>();
			language.addAll(driver.findElements(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[15]/div[3]/div/div[2]/div/div[1]/div[1]")));
			
			List<WebElement> languageValues = new ArrayList<>();
			languageValues.addAll(driver.findElements(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[15]/div[3]/div/div[2]/div/div[1]/div[2]")));
			for(int i = 1; i <= language.size(); i++)
			{
				System.out.println(language.get(i)+" = "+languageValues.get(i));
			}
		}
		else
		{
			System.out.println("Data loading failed!");
		}
				
		//Audience Brand Affinity
		System.out.println("**********Audience Brand Affinity**********");
		boolean brdAff = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[16]/div[1]/div/div[2]/div[1]")).isDisplayed();
		String brdAffinityText = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[16]/div[1]/div/div[1]/span")).getText();
		if(brdAff == true )
		{
			System.out.println("Data loaded successfully!");
			System.out.println(brdAffinityText);
			List<WebElement> brandAffinity = new ArrayList<>();
			brandAffinity.addAll(driver.findElements(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[16]/div[1]/div/div[2]/div[1]/div/div[1]")));
			
			List<WebElement> brandAffinityValues = new ArrayList<>();
			brandAffinityValues.addAll(driver.findElements(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[16]/div[1]/div/div[2]/div[1]/div/div[2]")));
			for(int i = 1; i <= brandAffinity.size(); i++)
			{
				System.out.println(brandAffinity.get(i)+" = "+brandAffinityValues.get(i));
			}
		}
		else
		{
			System.out.println("Data loading failed!");
		}
				
		//Audience Interests Affinity
		System.out.println("**********Audience Interests Affinity**********");
		boolean intrAff = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[16]/div[2]/div/div[2]")).isDisplayed();
		String intrAffinityText = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[16]/div[2]/div/div[1]")).getText();
		if(intrAff == true )
		{
			System.out.println("Data loaded successfully!");
			System.out.println(intrAffinityText);
			List<WebElement> interestsAffinity = new ArrayList<>();
			interestsAffinity.addAll(driver.findElements(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[16]/div[2]/div/div[2]/div/div[1]")));
			
			List<WebElement> interestsValues = new ArrayList<>();
			interestsValues.addAll(driver.findElements(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[16]/div[2]/div/div[2]/div/div[2]")));
			for(int i = 1; i <= interestsAffinity.size(); i++)
			{
				System.out.println(interestsAffinity.get(i)+" = "+interestsValues.get(i));
			}
		}
		else
		{
			System.out.println("Data loading failed!");
		}
				
		//Audience looklikes
		System.out.println("**********Audience looklikes**********");
		boolean audLook = driver.findElement(By.xpath("//*[@id=\"audience_lookalikes_list\"]")).isDisplayed();
		String audLookLikes = driver.findElement(By.xpath("//*[@id=\"audience_lookalikes_list\"]")).getText();
		String audLookText = driver.findElement(By.xpath("/html/body/div[5]/div[2]/div[2]/div[1]/div/div/div/div/div/div[16]/div[3]/div/div[1]/span")).getText();
		if(audLook == true )
		{
			rowcounter = 1;
			List<WebElement> audienceLooklikes = new ArrayList<>();
			for (WebElement webElement : audienceLooklikes)
			{
				audienceLooklikes = driver.findElements(By.xpath("//*[@id=\"audience_lookalikes_list\"]/div[\"+rowcount+\"]"));
				if(webElement.getText().equals(audienceLooklikes))
				{
					System.out.println(webElement);
				}
				rowcounter++;
			}
		}
		else
		{
			System.out.println("Data loading failed!");
		}
		
		driver.close();
		Thread.sleep(5000);
		driver.switchTo().window(tabs2.get(0));
		driver.findElement(By.xpath("/html/body/div[5]/div[2]/div/div/div[2]/div[2]/div/div/div/table/tbody/tr/td[6]/img")).click();
		Thread.sleep(2000);
		driver.findElement(By.id("delete-btn")).click();
	}
	
	public void Testcases_add_manually_influencer_tracking(String IG_handlename) throws InterruptedException
	{
		Thread.sleep(2000);
		influencertracking.click();
		Thread.sleep(2000);
		influencertracking_list.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//i[@class='fa fa-plus add_acc_plus']")));
		addplus.click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("recommended-influencer")));
		searchinput.sendKeys(IG_handlename);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//button[@class='btn-add-influencer-manually ']")).click();
		Thread.sleep(5000);
		  
		//For instagram
		driver.findElement(By.xpath("//input[@placeholder='https://www.instagram.com/nike/']")).sendKeys("https://www.instagram.com/rohitsharma45/");
			
		// for youtube
		driver.findElement(By.xpath("//input[@id='yt_infl']")).sendKeys("https://www.youtube.com/channel/UC544wgZVGXORbuaZskjc6PQ");
		  
		//for twitter
		driver.findElement(By.xpath("//input[@id='tw_infl']")).sendKeys("https://twitter.com/SrBachchan");
		driver.findElement(By.xpath("//button[@id='add-link']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//button[@id='add-link']")).click();
		Thread.sleep(2000);
	}
	
	// Brand Safety
	public void add_brand_safety_List() throws InterruptedException
	{
		Thread.sleep(2000);
		influencertracking.click();
		Thread.sleep(2000);
		brand_safety_report.click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//i[@class='fa fa fa-plus add_acc_plus']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@id='brandList_title']")).sendKeys("health");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='brandList_keyword']")));
		driver.findElement(By.xpath("//input[@id='brandList_keyword']")).sendKeys("health,exercise,fitness,bodystamina,immunity,booster,yoga,pushups,healthy");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@id='save_brandlist_data']")));
		driver.findElement(By.xpath("//button[@id='save_brandlist_data']")).click();
		Thread.sleep(5000);
	}
	
	public void Search_and_delete() throws InterruptedException
	{
		Thread.sleep(2000);
		influencertracking.click();
		Thread.sleep(2000);
		brand_safety_report.click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@name='serach-post']")).sendKeys("health");
		Thread.sleep(2000);
		driver.findElement(By.xpath("(//img[@class='edit-icon delete_brandSafety_list'])[1]")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//button[@id='delete-btn']")).click();
		Thread.sleep(5000);
	}
}