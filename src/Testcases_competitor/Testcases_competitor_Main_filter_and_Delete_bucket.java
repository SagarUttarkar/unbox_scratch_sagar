package Testcases_competitor;
import java.awt.AWTException;
import java.io.IOException;

import org.openqa.selenium.By;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import TestBase.Base_app;
import Utils.Testutils;
import modules.Competition;

public class Testcases_competitor_Main_filter_and_Delete_bucket extends Base_app
{
	modules.Competition comp;
	Testutils testutils;

	public Testcases_competitor_Main_filter_and_Delete_bucket()
	{
		super();
	}

	@BeforeMethod
	public void setup() throws IOException, AWTException, InterruptedException
	{
		Base_app.initialization();
		comp = new Competition();
	}
	
	@Test(priority=1)
	public void Testcase_competition_filter_FB() throws InterruptedException
	{
		comp.Testcases_main_competition_filter_FB();
	}
	
	@Test(priority=2)
	public void Testcase_competition_filter_TW() throws InterruptedException
	{
		comp.Testcases_main_competition_filter_TW();
	}
	
	@Test(priority=3)
	public void Testcase_competition_filter_YT() throws InterruptedException
	{
		comp.Testcases_main_competition_filter_YT();
	}
	
	@Test(priority=4)
	public void Testcase_competition_filter_IG() throws InterruptedException
	{
		comp.Testcases_main_competition_filter_IG();
	}

	/*@Test(priority=5)
	public void Testcase_competition_Account_delete() throws InterruptedException
	{
		comp.Testcases_delete_account_competion();
	}*/

	@AfterMethod
	public void teardown() throws InterruptedException
	{
		Thread.sleep(2000);
		driver.findElement(By.xpath("/html/body/div[3]/div[1]/div")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("/html/body/div[3]/div[3]/div[4]/a")).click();
		driver.close();
	}
}