package Testcases_Reports;
import java.awt.AWTException;
import java.io.IOException;

import org.openqa.selenium.By;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import TestBase.Base_app;
import Utils.Testutils;
import modules.Reports;

public class Testcases_Create_Reports_Competition_All_Platform extends Base_app
{
	modules.Reports reps;
	Testutils testutils;

	public Testcases_Create_Reports_Competition_All_Platform()
	{
		super();
	}

	@BeforeMethod
	public void setup() throws IOException, AWTException, InterruptedException
	{
		Base_app.initialization();
		reps = new Reports();
	}

	//Competition
	@Test(priority=1)
    public void Testcase_Create_Competitors_Report_Instagram1() throws InterruptedException
	{
		reps.Reports_add_comp_insta();
		System.out.println("Succsessfully generated Instagram Competition Report.");
	}

	@Test(priority=2)
    public void Testcase_Create_Competitors_Report_Facebook2() throws InterruptedException
	{
		reps.Reports_add_comp_fb();
		System.out.println("Succsessfully generated Facebook Competition Report.");
	}

	@Test(priority=3)
    public void Testcase_Create_Competitors_Report_Twitter3() throws InterruptedException
	{
		reps.Reports_add_comp_Twitter();
		System.out.println("Succsessfully generated Twitter Competition Report.");
	}

	@Test(priority=4)
    public void Testcase_Create_Competitors_Report_Youtube4() throws InterruptedException
	{
		reps.Reports_add_comp_Youtube();
		System.out.println("Succsessfully generated Youtube Competition Report.");
	}

	@AfterMethod
	public void teardown() throws InterruptedException
	{
		Thread.sleep(2000);
		driver.findElement(By.xpath("/html/body/div[3]/div[1]/div")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("/html/body/div[3]/div[3]/div[4]/a")).click();
		driver.close();
	}
}