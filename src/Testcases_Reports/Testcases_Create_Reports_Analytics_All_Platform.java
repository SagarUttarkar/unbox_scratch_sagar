package Testcases_Reports;
import java.awt.AWTException;
import java.io.IOException;

import org.openqa.selenium.By;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import TestBase.Base_app;
import Utils.Testutils;
import modules.Reports;

public class Testcases_Create_Reports_Analytics_All_Platform  extends Base_app
{
	//Testcases_Create_reports_Analytics_All_platform - 18 July 2023
	modules.Reports reps;
	Testutils testutils;

	public Testcases_Create_Reports_Analytics_All_Platform()
	{
		super();
	}

	@BeforeMethod
	public void setup() throws IOException, AWTException, InterruptedException
	{
		Base_app.initialization();
		reps = new Reports();
	}

	@Test(priority = 1)
	public void Testcase_Create_Analytics_Instagram_report() throws InterruptedException
	{
		reps.Reports_add_Instagram1();
		System.out.println("Succsessfully generated Instagram Analytics Report.");
	}

	@Test(priority = 2)
	public void Testcase_Create_Analytics_Facebook_report() throws InterruptedException
	{
		reps.Reports_add_Facebook2();
		System.out.println("Succsessfully generated Facebook Analytics Report.");
	}

	@Test(priority = 3)
	public void Testcase_Create_Analytics_Twitter_report() throws InterruptedException
	{
		reps.Reports_add_Twitter3();
		System.out.println("Succsessfully generated Twitter Analytics Report.");
	}

	@Test(priority = 4)
	public void Testcase_Create_Analytics_Yotube_reports() throws InterruptedException
	{
		reps.Reports_add_Youtube4();
		System.out.println("Succsessfully generated Youtube Analytics Report.");
	}

	@AfterMethod
	public void teardown() throws InterruptedException
	{
		Thread.sleep(2000);
		driver.findElement(By.xpath("/html/body/div[3]/div[1]/div")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("/html/body/div[3]/div[3]/div[4]/a")).click();
		driver.close();
	}
}